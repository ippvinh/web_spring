package com.ipv.web.models;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;

@Entity
@Table(name = "user")
public class User implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private long id;
	
    @Column(name = "email", nullable = false, unique = true)
    @NotEmpty(message = "{user.validate.emailNotEmply}")
    @Email(message = "{user.validate.email}")
    private String email;
    
    @Column(name = "password", nullable = false)
    @Length(min = 5, message = "{user.validate.lengthPassword}")
    @NotEmpty(message = "{user.validate.password}")
    private String password;
    
    @Column(name = "first_name", nullable = false)
    @NotEmpty(message = "{user.validate.firstName}")
    private String firstName;
    
    @Column(name = "last_name", nullable = false)
    @NotEmpty(message = "{user.validate.lastName}")
    private String lastName;
    
    @Column(name = "is_active", nullable = false)
    private boolean isActive;
    
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "user_role",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id")
    )
    private List<Role> roles;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "token_reset_password_id", referencedColumnName = "id")
    private TokenResetPassword tokenResetPassword;
    
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public TokenResetPassword getTokenResetPassword() {
		return tokenResetPassword;
	}

	public void setTokenResetPassword(TokenResetPassword tokenResetPassword) {
		this.tokenResetPassword = tokenResetPassword;
	}

	public User() {}

	public User(long id,
			@Email(message = "*Please provide a valid Email") @NotEmpty(message = "*Please provide an email") String email,
			@Length(min = 5, message = "*Your password must have at least 5 characters") @NotEmpty(message = "*Please provide your password") String password,
			@NotEmpty(message = "*Please provide your first name") String firstName,
			@NotEmpty(message = "*Please provide your last name") String lastName, boolean isActive, List<Role> roles) {
		this.id = id;
		this.email = email;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.isActive = isActive;
		this.roles = roles;
	}   	
}
