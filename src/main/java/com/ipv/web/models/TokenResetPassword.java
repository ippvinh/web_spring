package com.ipv.web.models;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "TokenResetPassword")
public class TokenResetPassword implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private long id;
	
	@NotEmpty
	@Column(name = "content", nullable = false)
	private String content;
	
	@Column(name = "valid_to", nullable = false)
	private LocalDateTime validTo;
	
	@OneToOne(mappedBy = "tokenResetPassword")
	private User user;
	
	public void validTo(int hours) {
		if (hours <= 0) {
			validTo = LocalDateTime.now();
		} else {
			validTo = LocalDateTime.now().plusHours(hours);
		}
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public LocalDateTime getValidTo() {
		return validTo;
	}

	public void setValidTo(LocalDateTime validTo) {
		this.validTo = validTo;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public TokenResetPassword() {}
}
