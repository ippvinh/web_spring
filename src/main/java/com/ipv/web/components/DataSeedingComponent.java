package com.ipv.web.components;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import com.ipv.web.models.Role;
import com.ipv.web.models.User;
import com.ipv.web.repository.RoleRepository;
import com.ipv.web.repository.UserRepository;

@Component
public class DataSeedingComponent implements ApplicationListener<ContextRefreshedEvent> {
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		addNewRoleIntoDatabase("ADMIN");
		addNewRoleIntoDatabase("POWER");
		addNewRoleIntoDatabase("USER");
		if (userRepository.findByEmail("admin@gmail.com") == null) {
			User admin = new User();
			admin.setEmail("admin@gmail.com");
			admin.setFirstName("Vinh");
			admin.setLastName("Phan");
			admin.setPassword(passwordEncoder.encode("admin"));
			List<Role> roles = new ArrayList<Role>();
			roles.add(roleRepository.findByName("ADMIN"));
			roles.add(roleRepository.findByName("POWER"));
			roles.add(roleRepository.findByName("USER"));
			admin.setRoles(roles);
			admin.setActive(true);
			userRepository.save(admin);
		}
	}
	private void addNewRoleIntoDatabase(String role) {
		if (roleRepository.findByName(role) == null) {
			roleRepository.save(new Role(role));
		}
	}
}