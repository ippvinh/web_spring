package com.ipv.web.controllers;

import java.time.LocalDateTime;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.ipv.web.models.User;
import com.ipv.web.services.UserService;
import com.ipv.web.models.Contact;
import com.ipv.web.models.TokenResetPassword;
import com.ipv.web.services.ContactService;
import com.ipv.web.services.EngineService;

@Controller
public class HomeController {
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private UserService userService;	
	@Autowired
	private ContactService contactService;
	@Autowired
	private EngineService engineService;
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView index() {
		ModelAndView modelAndView = new ModelAndView();
		boolean isSignin = false;
		String anonymousUser = "anonymousUser";
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String currentUserName = authentication.getName();	
		if(!currentUserName.equalsIgnoreCase(anonymousUser)) {
			isSignin = true;
		}
		modelAndView.addObject("userName", currentUserName);
		modelAndView.addObject("isSignin", isSignin);
		modelAndView.setViewName("index");
		logger.info("Welcome to index ...");
		return modelAndView;
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("login");
		logger.info("Welcome to login ...");
		return modelAndView;
	}
	
	@RequestMapping(value = "/InputEmailResetPassword", method = RequestMethod.GET)
	public ModelAndView getInputEmailResetPassword() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("InputEmailResetPassword");
		return modelAndView;
	}
	
	@RequestMapping(value = "/InputEmailResetPassword", method = RequestMethod.POST)
	public ModelAndView postInputEmailResetPassword(@RequestParam(value = "email") String email, HttpServletResponse response) {
		ModelAndView modelAndView = new ModelAndView();
		User user = userService.findUserByEmail(email);
		if(user != null) {
			Cookie cookie = new Cookie("email", email);	
			cookie.setHttpOnly(true);
		    response.addCookie(cookie);
		    TokenResetPassword token = user.getTokenResetPassword();
			if (token == null || LocalDateTime.now().isAfter(token.getValidTo())) {
				token = engineService.createDefaultTokenResetPassword();
				user.setTokenResetPassword(token);
				userService.saveUser(user);
				engineService.sendMail(email, token);
			} 
		    modelAndView.setViewName("redirect:/InputTokenResetPassword");
		}else {
			modelAndView.addObject("EmailError", messageSource.getMessage("resetPassword.emailError", null, LocaleContextHolder.getLocale()));
			modelAndView.setViewName("InputEmailResetPassword");
		}
		return modelAndView;
	}
	
	@RequestMapping(value = "/InputTokenResetPassword", method = RequestMethod.GET)
	public ModelAndView getInputTokenResetPassword(@CookieValue(value = "email", defaultValue = "a@a.com") String email){
		ModelAndView modelAndView = new ModelAndView();
		if(!email.equalsIgnoreCase("a@a.com")) {
			modelAndView.addObject("email", email);
			modelAndView.setViewName("InputTokenResetPassword");
		} else {
			modelAndView.setViewName("redirect:/login");
		}
		return modelAndView;
	}
	
	@RequestMapping(value = "/InputTokenResetPassword", method = RequestMethod.POST)
	public ModelAndView postInputTokenResetPassword(@CookieValue(value = "email", defaultValue = "a@a.com") String email, @RequestParam(value = "token") String token){
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("email", email);
		User user = userService.findUserByEmail(email);
		TokenResetPassword userToken = user.getTokenResetPassword();
		if(user == null || userToken == null || !userToken.getContent().equals(token) || LocalDateTime.now().isAfter(userToken.getValidTo())) {	
			modelAndView.addObject("TokenError", messageSource.getMessage("resetPassword.token.error", null, LocaleContextHolder.getLocale()));
			modelAndView.setViewName("InputTokenResetPassword");	
		}else {
			modelAndView.setViewName("redirect:/ResetPassword");	
		}		
		return modelAndView;
	}
	
	@RequestMapping(value = "/ResetPassword", method = RequestMethod.GET)
	public ModelAndView getResetPassword(@CookieValue(value = "email", defaultValue = "a@a.com") String email){
		ModelAndView modelAndView = new ModelAndView();
		if(!email.equalsIgnoreCase("a@a.com")) {
			modelAndView.addObject("email", email);
			modelAndView.setViewName("ResetPassword");
		} else {
			modelAndView.setViewName("InputEmailResetPassword");
		}
		return modelAndView;
	}
	
	@RequestMapping(value = "/ResetPassword", method = RequestMethod.POST)
	public ModelAndView postResetPassword(@CookieValue(value = "email", defaultValue = "a@a.com") String email, @RequestParam(value = "password") String password){
		ModelAndView modelAndView = new ModelAndView();
		if(password.isEmpty() || password.length() < 5) {
			modelAndView.addObject("PasswordError", messageSource.getMessage("resetPassword.newPassword.error", null, LocaleContextHolder.getLocale()));
		}else {
			User user = userService.findUserByEmail(email);
			user.setPassword(password);
			userService.saveUser(user);
			modelAndView.addObject("SuccessMessage", messageSource.getMessage("resetPassword.newPassword.success", null, LocaleContextHolder.getLocale()));
		}
		modelAndView.setViewName("ResetPassword");
		return modelAndView;
	}
	
	@RequestMapping(value = "/admin/home", method = RequestMethod.GET)
	public ModelAndView adminHome() {
		ModelAndView modelAndView = new ModelAndView();		
		modelAndView.setViewName("admin/home");
		return modelAndView;
	}
	
	@RequestMapping(value = "/registration", method = RequestMethod.GET)
	public ModelAndView registration() {
		ModelAndView modelAndView = new ModelAndView();
		User user = new User();
		modelAndView.addObject("user", user);
		modelAndView.setViewName("registration");
		return modelAndView;
	}
	
	@RequestMapping(value = "/registration", method = RequestMethod.POST)
	public ModelAndView createNewUser(@Valid User user, BindingResult bindingResult) {
		ModelAndView modelAndView = new ModelAndView();
		User userExists = userService.findUserByEmail(user.getEmail());
		if (userExists != null) {
			bindingResult.rejectValue("email", "error.user",
					messageSource.getMessage("user.validate.emailExist", null, LocaleContextHolder.getLocale()));
		}
		if (!bindingResult.hasErrors()) {
			userService.saveUser(user);
			modelAndView.addObject("successMessage", messageSource.getMessage("registration.success", null, LocaleContextHolder.getLocale()));
			modelAndView.addObject("user", new User());
		}
		modelAndView.setViewName("registration");
		return modelAndView;
	}
	
	@RequestMapping(value = "/contact", method = RequestMethod.GET)
	public ModelAndView getContact() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("contact", new Contact());
		modelAndView.setViewName("contact");
		return modelAndView;
	}

	@RequestMapping(value = "/contact", method = RequestMethod.POST)
	public ModelAndView postContact(@Valid Contact contact, BindingResult bindingResult) {
		ModelAndView modelAndView = new ModelAndView();
		if (!bindingResult.hasErrors()) {
			contactService.save(contact);			
			String content = contact.getName() + "\n" + contact.getEmail() + "\n" + contact.getPhone() + "\n"
					+ contact.getSubject() + "\n" + contact.getContent();
			engineService.sendMail("ipvinh@outlook.com", contact.getSubject(), content);
			modelAndView.addObject("successMessage", "User has been registered successfully");
		}
		modelAndView.setViewName("contact");
		return modelAndView;
	}
}