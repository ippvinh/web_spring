package com.ipv.web.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ipv.web.models.Contact;
import com.ipv.web.repository.ContactRepository;

@Service("contactService")
public class ContactService {
	@Autowired
	private ContactRepository repo;
	
	public List<Contact> listAll() {
		return repo.findAll();
	}

	public void save(Contact contact) {
		repo.save(contact);
	}

	public Contact get(long id) {
		return repo.findById(id).get();
	}

	public void delete(long id) {
		repo.deleteById(id);
	}	
}
