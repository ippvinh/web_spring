package com.ipv.web.services;

import java.time.format.DateTimeFormatter;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.ipv.web.models.TokenResetPassword;

@Service("engineService")
public class EngineService {
	
	@Autowired
	private JavaMailSender javaMailSender;
	private final char[] TOKEN_SAMPLES = {'0', '1', '2', '3', '4', '5', '6', '7', '8',  '9',  'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
			 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W',  'X', 'Y', 'Z'};
	
	public void sendMail(String toEmailAddress, String subject, String content) {
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(toEmailAddress);
		msg.setSubject(subject);
		msg.setText(content);
		javaMailSender.send(msg);
	}
	
	public void sendMail(String toEmailAddress, TokenResetPassword token) {
		String subject = "Token reset password from ipv";
		String content = "Token reset your password: "+ token.getContent() + "\n" + 
				"Valid to: "+ token.getValidTo().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(toEmailAddress);
		msg.setSubject(subject);
		msg.setText(content);
		javaMailSender.send(msg);
	} 
	public int createRandomIntegerInRange(int min, int max) {
	    Random r = new Random();
	    return r.nextInt((max - min) + 1) + min;
	}
	
	public String createRandomToken(int length) {
		String token = "A";
		if (!(length <= 0)) {
			char[] chars = new char[length];
			for(int counter = 0; counter < chars.length; counter++) {
				chars[counter] = TOKEN_SAMPLES[createRandomIntegerInRange(1, 35)];
			}
			token = new String(chars);
		}
		return token;
	}
	
	public TokenResetPassword createDefaultTokenResetPassword() {
		TokenResetPassword token = new TokenResetPassword();
		token.setContent(createRandomToken(6));
		token.validTo(12);
		return token;
	}
}
