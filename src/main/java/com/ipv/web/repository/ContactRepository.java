package com.ipv.web.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ipv.web.models.Contact;


public interface ContactRepository extends JpaRepository<Contact, Long> {

}
