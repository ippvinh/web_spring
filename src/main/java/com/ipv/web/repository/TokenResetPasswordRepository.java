package com.ipv.web.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ipv.web.models.TokenResetPassword;

@Repository("tokenResetPasswordRepository")
public interface TokenResetPasswordRepository extends JpaRepository<TokenResetPassword, Long>{

}
